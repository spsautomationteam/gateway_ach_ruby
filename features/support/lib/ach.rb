class ACH

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'rest_client'
  require "yaml"


  #@base_url
  @reference_num = "1"
  @content_type = ''
  @authorization=''

  #constructoe initialising yaml file
  def initialize()
    config = YAML.load_file('config.yaml')
    @base_url = config['url']
    @auth=config['authorization']
    puts "@base_url :#{@base_url}"
    set_url @base_url
  end

  #set authorization
  def set_authorization(mid, mkey)
    @authorization ="Bearer SPS-DEV-GW:test."+mid+"."+mkey
  end

  #set url
  def set_url(url)
    @url = url
  end

  #set headers
  def set_header(header, value)
    case header
      when "content-type"
        @content_type =value
      when 'Authorization'
        case value
          when 'Authorization'
            @authorization = @auth
          else
            @authorization =value
        end
    end
  end

  #set json body
  def set_json_body(jsonbody)
    @json_body = jsonbody
  end

  #Post txn with existing ref
  def post_existing_ref path
    post_ref path, @reference_num
  end

  #post txn with any reference
  def post_ref(path, ref)
#Debug urls
    puts "path#{path}"
    full_url= @url+path

    if (!ref.empty?)
      puts "ref#{ref}"
      full_url+="/" +ref
    end

    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"

#Do post action and get response
    begin
      @response = RestClient.post full_url, @json_body, :Authorization => @authorization, :content_type => @content_type
      @reference_num = JSON.parse(@response.body)['reference']
      puts "@reference_num: #{@reference_num}"
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    rescue RestClient::Unauthorized => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
    puts "response: #{@response}"
  end

  #patch txn
  def patch(path)
    puts "#patch request"
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"
    puts "@reference_num:#{@reference_num}"

    #Do post action and get response
    begin
      @response = RestClient.patch full_url+"/"+@reference_num, @json_body, :Authorization => @authorization, :content_type => @content_type
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
      resce RestClient::UnsupportedMediaType => err
      @response = err.response
    end

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end

    # puts "response : #{@respons}"
  end

  #patch txn with any reference
  def patch_ref(path, reference_num)
    puts "#patch_ref "
    #Debug urls
    puts "path#{path}"
    full_url= @url+path
    puts "@full_url: #{full_url}"
    puts "@json_body: #{@json_body}"
    puts "@authorization: #{@authorization}"
    puts "@content_type: #{@content_type}"
    puts "reference_num:#{reference_num}"

    #Do post action and get response
    begin
      @response = RestClient.patch full_url+"/"+reference_num, @json_body, :Authorization => @authorization, :content_type => @content_type
    rescue RestClient::BadRequest => err
      @response = err.response
    rescue RestClient::NotFound => err
      @response = err.response
    rescue RestClient::UnsupportedMediaType => err
      @response = err.response
    end
    # puts "response: #{@response}"

    if (@response.body.length !=0)
      @parsed_response = JSON.parse(@response.body)
    end
  end

  #Verifying txn code
  def verify_txn_code(code)
    expect(@response.code.to_s).to eql(code)
  end

  #Verifying status
  def verify_status(status)
    expect(@parsed_response['status']).to eql("Approved"), "Expected : Approved ,got : #{@parsed_response['status']}\n Responesbody: #{@parsed_response}"
  end

  #verify the response contain a substring
  def verify_response_params_contain(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if (response_param == "code")
      expect(@response.code.to_s.include? value).to be_truthy, "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
    else
      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param].include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
      else
        expect(@parsed_response[response_param].strip.include? value).to be_truthy, "Expected : #{value} ,got : #{@parsed_response[response_param].strip} \nResponesbody:#{@parsed_response}"
      end
    end
  end

  #verifying response params
  def verify_response_params(response_param, value)
    puts "response_param:#{response_param} , value:#{value}"

    if (response_param == "code")
      expect(@response.code.to_s).to eql(value), "Expected : #{value} ,got : #{@response.code.to_s} \nResponesbody:#{@parsed_response}"
    else
      if @parsed_response[response_param].nil?
        expect(@parsed_response[response_param]).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param]} \nResponesbody:#{@parsed_response}"
      else
        expect(@parsed_response[response_param].strip).to eql(value), "Expected : #{value} ,got : #{@parsed_response[response_param].strip} \nResponesbody:#{@parsed_response}"
      end
    end
  end

  #verifying response headers
  def verify_response_headers(header_param, value)
    puts "header_param:#{header_param} , value:#{value}"
    expect(@response.headers[:content_type]).to include(value), "Expected : #{value} ,got : #{@response.headers[:content_type]} \nResponesbody:#{@parsed_response}"
  end
  
end
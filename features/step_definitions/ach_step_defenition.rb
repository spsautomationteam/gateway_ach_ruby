Given(/^I set Authorization header with MID as (.*) and MKEY as (.*)$/) do |mid, mkey|
  @ach.set_authorization mid, mkey
end

And(/^I set url as (.*)$/) do |url|
  @ach.set_url url
end

And(/^I set (.*) header to (.*)$/) do |header, value|
  @ach.set_header header, value
end

And(/^I set body to (.*)$/) do |json|
  @ach.set_json_body json
end

And(/^I post data for transaction (.*)$/) do |path|
  @ach.post_ref path, ""
end
And(/^I post with existing reference data for transaction (.*)$/) do |path|
  @ach.post_existing_ref path
end

And(/^I post with reference as (.*) data for transaction (.*)$/) do |ref, path|
  @ach.post_ref path, ref
end

And(/^I patch data for transaction (.*)$/) do |path|
  @ach.patch path
end

And(/^response header (.*) should be (.*)$/) do |header_param, val|
  @ach.verify_response_headers header_param, val
end

Then(/^response body path (.*) should be (.*)$/) do |param, val|
  @ach.verify_response_params param, val
end

Then(/^response body path (.*) should contain (.*)$/) do |param, val|
  @ach.verify_response_params_contain param, val
end

And(/^I patch data with reference as (.*) for transaction (.*)$/) do |reference, path|
  @ach.patch_ref path, reference
end

Then(/^response code should be (.*)$/) do |responsecode|
  @ach.verify_response_params "code", responsecode
end